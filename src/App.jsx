import "./App.scss";
import React from "react";
import HeaderBar from 'components/header-bar/header-bar.component';
import HistoricPage from "page/historic/historic.component";
import {Route, Switch, Redirect} from "react-router-dom";
import MapPage from "page/map/map.component";
import EvenementPage from "page/evenement/evenement.component";
import RecordDataPage from "page/record-data/record-data.component";

class App extends React.Component {
  render() {
    return (
      <div>
        <HeaderBar/>
        <div className="app-body">
          {/* <HistoricPage/> */}
          <Switch>
            <Route exact path="/" component={HistoricPage}/>
            <Route path="/regions" component={MapPage} />
            <Route path="/evenement" component={EvenementPage} />
            <Route path="/record-data" component={RecordDataPage} />
            <Redirect to="/" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
