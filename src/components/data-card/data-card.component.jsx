import React from 'react';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './data-card.styles';
import lineGraph from 'assets/photo/line-graph.png'
import { Link } from "react-router-dom";

export default function DataCard() {
  const classes = useStyles();

  return (
    <Card className={classes.root} elevation={3}> 
      <CardActionArea component={Link} to="/record-data" className={classes.cardAction}>
        <div className={classes.cover}>
          <CardMedia
            className={classes.media}
            src={lineGraph}
            component="img"
            title="Map France"
          />
        </div>
        <div className={classes.details}>
         <CardContent className={classes.content}>
          <Typography component="h5" variant="h5" gutterBottom>
            Graphiques température et précipitation
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Dans cette section vous retrouverrez par ville les températures minimum, maximun et les précipitations
              des années passées.
          </Typography>
         </CardContent>
        </div>
      </CardActionArea>
    </Card>
  );
}
