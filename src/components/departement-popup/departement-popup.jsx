import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import {Drawer} from "@material-ui/core";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper, Button } from '@material-ui/core'
import Typography from '@material-ui/core/Typography';
import Carousel from 'react-material-ui-carousel'
import CardMedia from '@material-ui/core/CardMedia';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
const useStyles = makeStyles({
  list: {
    width: 750,
    margin: '0 2rem 2rem 2rem'
  },
  fullList: {
    width: "auto"
  }
});

export default function TemporaryDrawer({currentLoc}) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    right: false
  });

  console.log('j ai')
  console.log(currentLoc)
  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom"
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      // onKeyDown={toggleDrawer(anchor, false)}
    >
    <br />
    <br />
    <br />
    <Typography gutterBottom variant="h5" component="h2" align="center">
        Departement {currentLoc.name}
    </Typography>
    <br />
    <br />
    <h4>Données évenement</h4>
    <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell align="right">anecdotique</TableCell>
                <TableCell align="right">notable</TableCell>
                <TableCell align="right">remarquable</TableCell>
                <TableCell align="right">exceptionnel</TableCell>
                <TableCell align="right">mémorable</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                <TableRow key={1}>
                <TableCell align="right">{currentLoc.anecdotique}</TableCell>
                <TableCell align="right">{currentLoc.notable}</TableCell>
                <TableCell align="right">{currentLoc.remarquable}</TableCell>
                <TableCell align="right">{currentLoc.exceptionnel}</TableCell>
                <TableCell align="right">{currentLoc.memorable}</TableCell>
                </TableRow>

            </TableBody>
        </Table>
    </TableContainer>
        <br />
        <Carousel>
            {
                currentLoc.listeLien.map( (item, i) => <Item key={i} item={item} /> )
            }
        </Carousel>

    </div>
  );

  return (
    <div>
      {[ "right"].map((anchor) => (
        <React.Fragment key={"right"}>
          <Button onClick={toggleDrawer("right", true)}>+ d'info</Button>
          <Drawer
            anchor={"right"}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}


const useStylesC = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  media:{
    height: "400px",
  }
});
function Item(url)
{
  console.log("url")
  console.log(url.item.lien)
  const classes = useStylesC();
  // const bull = <span className={classes.bullet}>•</span>;

    return (
      <Card className={classes.root} variant="outlined">
      <CardContent>
          {
              url.item.ville ? (
                  <Typography variant="h5" align="center">
                      Ville: {url.item.ville}
                  </Typography>
              ) : ''
          }

        <CardMedia
                // className={classes.media}
                image={"https://www.infoclimat.fr/photolive/photos/"+url.item.lien}
                title="photo paysage"
                className={classes.media}
        />
      </CardContent>
    </Card>
         
      
    )
}