import React from "react";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Typography from "@material-ui/core/Typography";
import { useStyles } from "./descript-dialog.styles";
import PostIcon from "assets/svg/post.svg";
import PlaceHolder from "assets/svg/placeholder.svg";
import Calendar from "assets/svg/calendar.svg";
import Paper from "@material-ui/core/Paper";

export default function DescripDialog(props) {
  const classes = useStyles();

  const { onClose, rowData, open } = props;
  console.log(rowData);
  return (
    <Dialog
      onClose={onClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      maxWidth="md"
      className={classes}
    >
      <DialogContent>
        <Typography color="textSecondary" component="h6" variant="h6">
          Type d'évenement : {rowData.type}
        </Typography>
        <div className={classes.title}>
          <img src={PostIcon} alt="alert" className={classes.icon} />
          <Typography component="h5" variant="h5" className={classes.titleText}>
            {rowData.nom}
          </Typography>
        </div>
        <div className={classes.subtitle}>
          <img
            src={Calendar}
            alt="period"
            className={classes.placerholderIcon}
          />
          <Typography variant="subtitle1">
            Date (début - fin): {rowData.date_deb} - {rowData.date_fin}
          </Typography>
        </div>
        <div className={classes.subtitle}>
          <img
            src={PlaceHolder}
            alt="location"
            className={classes.placerholderIcon}
          />
          <Typography variant="subtitle1">{rowData.localisation}</Typography>
        </div>
        <Paper className={classes.paper}>
          <DialogContentText>{rowData.short_desc}</DialogContentText>
        </Paper>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}
