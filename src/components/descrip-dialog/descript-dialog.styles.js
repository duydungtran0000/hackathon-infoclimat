import {
    makeStyles
} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
        maxWidth: 800,
        maxHeight: 600
    },
    title: {
        display: "flex",
        margin: "1.25rem 0",
        alignItems: "Center"
    },
    icon: {
        width: 32,
        marginBottom: 12
    },
    titleText: {
        marginLeft: "1rem"
    },
    subtitle: {
        display: "flex",
        alignItems: "Center",
    },
    placerholderIcon: {
        width: 20,
        marginBottom: 9,
        marginRight: "0.25rem"
    },
    paper: {
        padding: "1rem",
        marginTop: "2rem"
    }
});