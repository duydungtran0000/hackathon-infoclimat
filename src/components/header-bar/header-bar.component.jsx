import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import CssBaseline from "@material-ui/core/CssBaseline";
import { useStyles, ElevationScroll } from "./header-bar.styles";
import { Link } from "react-router-dom";
import Logo from "assets/photo/logo.png"

export default function ElevateAppBar(props) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <ElevationScroll {...props}>
        <AppBar className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <Link to="/">
              <img src={Logo} alt="infoclimat-logo" className={classes.logo}/>
            </Link>
            <div className={classes.subMenu}>
              <Typography variant="h6">Temps Réels</Typography>
              <Typography variant="h6">Communauté</Typography>
              <Typography variant="h6">Pédagogie</Typography>
              <Typography variant="h6">Forums</Typography>
            </div>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />
    </React.Fragment>
  );
}
