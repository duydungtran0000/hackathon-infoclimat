import {
    makeStyles
} from '@material-ui/core/styles';
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import PropTypes from "prop-types";
import React from "react";

export const useStyles = makeStyles(() => ({
    appBar: {
        backgroundColor: '#fff',
        color: 'black',
        boxShadow: '0 0 20px 0 rgb(46 53 57 / 11%)',
    },
    title: {
        flexGrow: 1,
    },
    logo: {
        maxWidth: "6rem",
        cursor: "pointer"
    },
    toolbar: {
        padding: "5px 50px 5px 50px",
        alignItems: 'center',
        display: 'flex',
        height: '6em',
        justifyContent: 'space-between'
    },
    subMenu: {
        display:'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        // width: '100%'
        minWidth: '40%'
    }
}));

export function ElevationScroll(props) {
    const {
        children,
        window
    } = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
        target: window ? window() : undefined,
    });

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0,
    });
}

ElevationScroll.propTypes = {
    children: PropTypes.element.isRequired,
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};