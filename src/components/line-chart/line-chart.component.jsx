import React from "react";
import ReactApexChart from "react-apexcharts";
import { getMaxTemps, getMinTemps } from "./line-chart.utils";

class LineChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      series: [
        {
          name: "Température maximale (°C)",
          data: getMaxTemps(this.props.data),
        },
        {
          name: "Température minimale (°C)",
          data: getMinTemps(this.props.data),
        }
      ],
      options: {
        chart: {
          height: 400,
          type: "line",
          dropShadow: {
            enabled: true,
            color: "#000",
            top: 18,
            left: 7,
            blur: 10,
            opacity: 0.2,
          },
          toolbar: {
            show: false,
          },
          zoom: {
            enabled: false,
          },
        },
        colors: ["#ff445e", "#77B6EA"],
        dataLabels: {
          enabled: true,
        },
        stroke: {
          curve: "smooth",
        },
        title: {
          text: "Température max & min du mois sur la périod 1981-2010",
          align: "left",
        },
        grid: {
          borderColor: "#e7e7e7",
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5,
          },
        },
        markers: {
          size: 1,
        },
        xaxis: {
          categories: [
            "Janv",
            "Févr",
            "Mars",
            "Avr",
            "Mai",
            "Juin",
            "Juill",
            "Août",
            "Sept",
            "Oct",
            "Nov",
            "Déc",
          ],
          title: {
            text: "Mois",
          },
        },
        yaxis: {
          title: {
            text: "Températures, Précipitations",
          },
          min: -10,
          max: 40,
        },
        legend: {
          position: "top",
          horizontalAlign: "right",
          floating: true,
          offsetY: -25,
          offsetX: -5,
        },
      },
    };
  }

  componentDidUpdate(prevProps) {
    console.log(this.props);
    if (prevProps.data !== this.props.data) {
      this.setState({
        ...this.state,
        series: [
          {
            name: "High - 2013",
            data: getMaxTemps(this.props.data),
          },
          {
            name: "Low - 2013",
            data: getMinTemps(this.props.data),
          },
        ],
      });
    }
  }
  render() {
    return (
      <div>
        <ReactApexChart
          options={this.state.options}
          series={this.state.series}
          type="line"
          height={450}
        />
      </div>
    );
  }
}

export default LineChart;
