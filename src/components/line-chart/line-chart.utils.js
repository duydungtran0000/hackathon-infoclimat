export const getMaxTemps = (data) => {
    const maxTemps = data.map(d => {
        if (Object.keys(d).indexOf('tx') > -1) {
            return d.tx
        } else {
            return null
        }
    })
    console.log(maxTemps)
    return maxTemps
}

export const getMinTemps = (data) => {
    const minTemps = data.map(d => {
        if (Object.keys(d).indexOf('tn') > -1) {
            return d.tn
        } else {
            return null
        }
    })
    console.log(minTemps)
    return minTemps
}

export const getPrecipitations = (data) => {
    const precips = data.map(d => {
        if (Object.keys(d).indexOf('precip') > -1) {
            return d.precip
        } else {
            return null
        }
    })
    console.log(precips)
    return precips
}