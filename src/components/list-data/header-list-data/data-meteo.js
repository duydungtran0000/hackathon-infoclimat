export const dataMeteo = [
    {
        numero: 1,
        type: 'froid inhabituel'
    },
    {
        numero: 2,
        type: 'épisode neigeux'
    },
    {
        numero: 3,
        type: 'douceur inhabituelle'
    },
    {
        numero: 4,
        type: 'épisode pluvieux'
    },
    {
        numero: 5,
        type: 'inondation'
    },
    {
        numero: 6,
        type: 'tempête/coup de vent'
    },
    {
        numero: 7,
        type: 'cyclone'
    },
    {
        numero: 8,
        type: 'chaleur / canicule'
    },
    {
        numero: 9,
        type: 'orages'
    },
    {
        numero: 10,
        type: 'sécheresse'
    },
    {
        numero: 11,
        type: 'pluies verglaçantes'
    },
    {
        numero: 12,
        type: 'gelées tardives'
    },
    {
        numero: 13,
        type: 'gelées précoces'
    },
    {
        numero: 14,
        type: 'froid'
    },
    {
        numero: 15,
        type: 'grêle'
    },
    {
        numero: 16,
        type: 'épisode neigeux tardif'
    },
    {
        numero: 17,
        type: 'épisode neigeux précoce'
    },
    {
        numero: 18,
        type: 'tornade'
    },
    {
        numero: 19,
        type: 'dépression extra-tropicale'
    },
    {
        numero: 20,
        type: 'tempête tropicale'
    },
    {
        numero: -1,
        type: 'autres'
    }
]