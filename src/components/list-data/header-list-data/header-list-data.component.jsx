import React from "react";
import Link from "@material-ui/core/Link";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Typography from "@material-ui/core/Typography";
import Navigation from "../navigation/navigation.component";

const HeaderListData = () => {
  return (
    <div>
      <Breadcrumbs aria-label="breadcrumb">
        <Link color="inherit" href="/">
          <Typography variant="h5">HistorIc</Typography>
        </Link>
        <Link color="inherit" href="/evenement">
          <Typography variant="h5">Record d'Événements</Typography>
        </Link>
      </Breadcrumbs>
      <Navigation />
    </div>
  );
};

export default HeaderListData;
