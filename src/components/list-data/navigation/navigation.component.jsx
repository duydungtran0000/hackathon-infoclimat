import React from "react";
import "./navigation.styles.scss";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";

const Navigation = ({ match, history }) => {
  return (
    <div className="navigation">
      <ul>
        <li>
          <Link to={`${match.url}`}>Nouveau</Link>
        </li>
        <li>
          <Link to={`${match.url}/popular`}>Populaire</Link>
        </li>
      </ul>
    </div>
  );
};

export default withRouter(Navigation);
