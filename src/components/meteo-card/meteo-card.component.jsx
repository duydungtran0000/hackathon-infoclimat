import React from "react";

import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { useStyles } from "./meteo-card.styles";
import meteoImg from "assets/photo/meteo.png";
import { Link } from "react-router-dom";

export default function MeteoCard() {
  const classes = useStyles();

  return (
    <Card className={classes.root} elevation={3}>
      <CardActionArea component={Link} to="/evenement" className={classes.cardAction}>
        <div className={classes.cover}>
          <CardMedia
            className={classes.media}
            src={meteoImg}
            component="img"
            title="Map France"
          />
        </div>
        <div className={classes.details}>
         <CardContent className={classes.content}>
          <Typography component="h5" variant="h5" gutterBottom>
            Données importantes et populaires
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Dans cette section vous trouverrez les données les plus importantes par polurarité et par date d'ajout.
              Vous pourrez aussi filtrer les données par types d'événements.
          </Typography>
         </CardContent>
        </div>
      </CardActionArea>
    </Card>
  );
}
