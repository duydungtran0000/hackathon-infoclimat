import React from "react";
import ReactApexChart from "react-apexcharts";
import { getPrecipitations } from "./precip-chart.utils";

class PrecipChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      series: [
        {
          name: "Précipitations (mm)",
          data: getPrecipitations(this.props.data),
        },
      ],
      options: {
        chart: {
          height: 400,
          type: "area",
          toolbar: {
            show: false,
          },
          zoom: {
            enabled: false,
          },
        },
        colors: ["#008ffb"],
        dataLabels: {
          enabled: true,
        },
        stroke: {
          curve: "smooth",
        },
        title: {
          text: "Average Précipitations du mois sur la prériod 1981-2010",
          align: "left",
        },
        grid: {
          borderColor: "#e7e7e7",
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5,
          },
        },
        markers: {
          size: 1,
        },
        xaxis: {
          categories: [
            "Janv",
            "Févr",
            "Mars",
            "Avr",
            "Mai",
            "Juin",
            "Juill",
            "Août",
            "Sept",
            "Oct",
            "Nov",
            "Déc",
          ],
          title: {
            text: "Mois",
          },
        },
        yaxis: {
          title: {
            text: "Températures, Précipitations",
          },
          min: 0,
          max: 150,
        },
        legend: {
          position: "top",
          horizontalAlign: "right",
          floating: true,
          offsetY: -25,
          offsetX: -5,
        },
      },
    };
  }

  componentDidUpdate(prevProps) {
    console.log(this.props);
    if (prevProps.data !== this.props.data) {
      this.setState({
        ...this.state,
        series: [
          {
            name: "Précipitations (1981-2010)",
            data: getPrecipitations(this.props.data),
          }
        ],
      });
    }
  }

  render() {
    return (
      <div>
        <ReactApexChart
          options={this.state.options}
          series={this.state.series}
          type="line"
          height={450}
        />
      </div>
    );
  }
}

export default PrecipChart;
