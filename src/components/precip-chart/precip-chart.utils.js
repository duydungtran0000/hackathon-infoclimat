export const getPrecipitations = (data) => {
    const precips = data.map(d => {
        if (Object.keys(d).indexOf('precip') > -1) {
            return d.precip
        } else {
            return null
        }
    })
    console.log(precips)
    return precips
}