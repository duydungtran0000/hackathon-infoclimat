import React from 'react';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './region-card.styles';
import mapFrance from 'assets/photo/carte-france.png'
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";

const RegionCard = ({match, history }) =>{
  const classes = useStyles();
  console.log(match)
  console.log(history)
  return (
    <Card className={classes.root} elevation={3}>
      <CardActionArea component={Link} to="/regions" className={classes.cardAction}>
        <div className={classes.cover}>
          <CardMedia
            className={classes.media}
            src={mapFrance}
            component="img"
            title="Map France"
          />
        </div>
        <div className={classes.details}>
         <CardContent className={classes.content}>
          <Typography component="h5" variant="h5" gutterBottom>
            Départements
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
              Dans cette section, vous trouverez une carte de la France avec les différents départements que vous pourrez sélectionner
              et avoir les informations importantes de température.
          </Typography>
         </CardContent>
        </div>
      </CardActionArea>
    </Card>
  );
}

export default withRouter(RegionCard);
