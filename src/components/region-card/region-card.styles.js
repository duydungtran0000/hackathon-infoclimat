import {
    makeStyles
} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
        maxWidth: 750,
    },
    media: {
        height: 250,
        width: 250,
        padding: "1rem"
    },
    cardAction: {
        display: 'flex',
        padding: "1rem",
        justifyContent: "start"
    },
    content: {
        flex: '1 0 auto',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    cover: {
        width: 600,
    }
});