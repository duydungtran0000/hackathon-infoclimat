import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import Paper from "@material-ui/core/Paper";
import { useStyles2 } from "./topic-table.styles";
import { TablePaginationActions } from "./table-pagination-actions";

const HitTable = ({headerRows, rows}) => {
    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const emptyRows =
        rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <TableContainer className={classes.tableContainer} component={Paper}>
            <Table className={classes.table} aria-label="custom pagination table">
                <TableHead className={classes.tHead}>
                    <TableRow>
                        {headerRows.map((column, id) =>
                            id === 0 ? (
                                <TableCell key={id}>{column}</TableCell>
                            ) : (
                                <TableCell key={id} align="right">
                                    {column}
                                </TableCell>
                            )
                        )}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {(rowsPerPage > 0
                            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            : rows
                    ).map((row) => (
                        <TableRow key={row.name}>
                            <TableCell component="th" scope="row" className={classes.tColumn}>
                                {row.nom}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.localisation}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.date_deb}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.date_fin}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.type}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.hits}
                            </TableCell>
                        </TableRow>
                    ))}

                    {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                            <TableCell colSpan={6} />
                        </TableRow>
                    )}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            className={classes.tablePagination}
                            colSpan={3}
                            rowsPerPageOptions={[]}
                            count={rows.length}
                            rowsPerPage={rowsPerPage}
                            labelRowsPerPage=""
                            page={page}
                            SelectProps={{
                                inputProps: { "aria-label": "rows per page" },
                                native: true,
                            }}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
};

export default HitTable;
