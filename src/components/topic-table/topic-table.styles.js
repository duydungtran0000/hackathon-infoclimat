
import { makeStyles } from '@material-ui/core/styles';

export const useStyles1 = makeStyles((theme) => ({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
    },
}));

export const useStyles2 = makeStyles({
    table: {
      minWidth: 500,
    },
    tableContainer: {
      boxShadow: "none"
    },
    tablePagination: {
      borderBottom: "none"
    },
    tHead: {
        opacity: 0.75,
        color: "red",
        fontSize: '14px',
        fontWeight: 300,
        lineHeight: "29px",
        fontFamily: "Montserrat,  sans-serif"
    },
    tColumn: {
        fontSize: "15px",
        fontWeight: 600,
        lineHeight: "22px",
        fontFamily: "'Montserrat', sans-serif",
        color: "#000000 !important",
        cursor: "pointer"
    }
  });