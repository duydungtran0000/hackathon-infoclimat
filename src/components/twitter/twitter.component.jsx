import { TwitterTimelineEmbed } from "react-twitter-embed";

export default function Twitter() {
  return (
    <div>
      <TwitterTimelineEmbed
        sourceType="profile"
        // The preferred screen name goes next:
        screenName="infoclimat"
        // Style options goes here:
        options={{ height: '90vh', width: '100vh' }}
      />
    </div>
  );
}
