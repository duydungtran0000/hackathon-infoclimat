import React from "react";
import HeaderListData from "components/list-data/header-list-data/header-list-data.component";
import LastestTopicPage from "page/lastest-topic/lastest-topic.component";
import PopularTopicPage from "page/popular-topic/popular-topic.component";
import { Route, Switch } from "react-router-dom";
import "./evenement.styles.scss";
class EvenementPage extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);

    this.state = {
      currentUrl: this.props.match.url,
    };
  }

  render() {
    return (
      <div className="evenement">
        <HeaderListData />
        <Switch>
          <Route
            exact
            path={`${this.state.currentUrl}`}
            component={LastestTopicPage}
          />
          <Route
            path={`${this.state.currentUrl}/popular`}
            component={PopularTopicPage}
          />
        </Switch>
      </div>
    );
  }
}

export default EvenementPage;
