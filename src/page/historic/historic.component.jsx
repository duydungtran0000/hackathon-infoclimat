import "./historic.styles.scss";
import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

import RegionCard from "components/region-card/region-card.component";
import MeteoCard from "components/meteo-card/meteo-card.component";
import DataCard from "components/data-card/data-card.component";
import Twitter from "components/twitter/twitter.component";
import InfoAlert from 'components/info-bar/info-bar'
import AlertIcon from "assets/svg/alert.svg";

class HistoricPage extends React.Component {
  render() {
    return (
      <div className="historic-wrapper">
        <InfoAlert/>
        <h2>HistorIc</h2>
        <Typography>
          HistorIC, c'est l'archivage de 2729 événements météorologiques
          extrêmes en France et 224919 séries de valeurs depuis 1653.
        </Typography>
        <div className="div-wrap">
          <div className="historic-cards">
            <div className="card-item">
              <RegionCard />
            </div>
            <div className="card-item">
              <MeteoCard className="card-item" />
            </div>
            <div className="card-item">
              <DataCard className="card-item" />
            </div>
          </div>
          <Paper style={{ width: "35%" }} elevation={3}>
            <div className="paper-content">
              <div className="paper-title">
                <img src={AlertIcon} alt="alert" className="alert-icon"/>
                <Typography variant="h4" style={{ margin: "0 2rem"}}>
                  Actu Twitter
                </Typography>
                <img src={AlertIcon} alt="alert" className="alert-icon"/>
              </div>
              <div className="news-twitter-content">
                <Twitter />
              </div>
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}

export default HistoricPage;
