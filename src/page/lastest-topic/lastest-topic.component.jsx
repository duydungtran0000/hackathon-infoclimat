import React from "react";
// import HeaderListData from "components/list-data/header-list-data/header-list-data.component";
import TopicTable from "components/topic-table/topic-table.component";
import CircularProgress from "@material-ui/core/CircularProgress";
import axios from "axios";
import { meteoData } from "./dummy-data";
import { cleanData, filterDataByTypes } from "./utils";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

class LastestTopicPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      headerRows: [
        "Nom",
        "Localisation",
        "Date de début",
        "Date de fin",
        "Type d'événement",
      ],
      isLoading: true,
      datas: null,
      dataFiltered:null,
      typeSelected: ''
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_URL}/api/HistoricEvents`)
      .then((response) => {
        console.log(response);
        const resData = response.data.datas;
        cleanData(resData)
        if (response.status === 200) {
          this.setState({
            isLoading: false,
            datas: resData,
            dataFiltered: resData
          });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      ...this.state,
      [name]: value
    }, function () {
        if (this.state.typeSelected !== "") {
          const data = filterDataByTypes(
            this.state.datas,
            this.state.typeSelected
          );
          this.setState({
            ...this.state,
            dataFiltered: data,
          });
        } else {
          this.setState({
            ...this.state,
            dataFiltered : this.state.datas
          })
        }
      })
  }

  render() {
    return (
      <div>
        <FormControl variant="outlined">
          <InputLabel htmlFor="outlined-city-native-simple">
            Choisir le type d'événement
          </InputLabel>
          <Select
            native
            value={this.state.typeSelected}
            onChange={(e) => this.handleChange(e)}
            label="Type d'événement"
            inputProps={{
              name: "typeSelected",
              id: "outlined-type-native-simple",
            }}
          >
            <option aria-label="None" value="" />
            {meteoData.map((meteo, id) => (
              <option key={id} value={meteo.type}>
                {meteo.type}
              </option>
            ))}
          </Select>
        </FormControl>
        <div>
        {this.state.isLoading ? (
          <CircularProgress />
        ) : (
          <TopicTable
            headerRows={this.state.headerRows}
            rows={this.state.dataFiltered}
          />
        )}
        </div>
      </div>
    );
  }
}

export default LastestTopicPage;
