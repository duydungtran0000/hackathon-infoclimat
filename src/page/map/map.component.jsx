import "./map.styles.scss";
import { React, useState } from "react";
import France from "@svg-maps/france.departments";
import { CheckboxSVGMap } from "react-svg-map";
import "react-svg-map/lib/index.css";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import axios from "axios";
import TemporaryDrawer from "../../components/departement-popup/departement-popup";

function MapPage() {
  const [location, setLocaltion] = useState([]);

  function getLocationInfo(loc) {
    console.log(loc.map((elem) => elem.id));
    var config = {
      method: "get",
      url: `http://localhost:8000/api/getInfoByDepats?liste=${loc.map(
        (elem) => elem.id
      )}`,
    };

    axios(config)
      .then(function (response) {
        response.data.map(
          (loca, index) => (loca["name"] = loc[index].ariaLabel)
        );
        setLocaltion(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  return (
    <div className="wrap">
      <div className="crumbs">
        <Breadcrumbs aria-label="breadcrumb">
          <Link color="inherit" href="/">
            <Typography variant="h5">HistorIc</Typography>
          </Link>
          <Link color="inherit" href="/regions">
            <Typography variant="h5">Carte</Typography>
          </Link>
        </Breadcrumbs>
      </div>
      <div className="inline">
        <div className="middle">
          <CheckboxSVGMap
            map={France}
            onChange={(loc) => getLocationInfo(loc)}
          />
        </div>
        <div className="middle listCard">
          {location.map((value, index) => {
            return <Card key={index}>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
              {value.name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="h4">
                Departement {value.id}
              </Typography>
              
              <Typography variant="body2" color="textSecondary" component="p">
                Temperature maximal enregistré : { value.tx__max}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
              Temperature minimal enregistré : { value.tn__min}

              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
              Nombre d'evenement enregisté : { value.count}

              </Typography>
              
            </CardContent>
            <CardActions>
              {/* <Button size="small">+ d'info</Button> */}
              <TemporaryDrawer currentLoc={value}/>  
            </CardActions >
          </Card> 
          })}
          
          </div>
        
        </div>
      </div>

  );
}

export default MapPage;
