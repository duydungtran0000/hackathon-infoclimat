 export const meteoData = [
    {
        numero: 1,
        type: 'froid inhabituel'
    },
    {
        numero: 2,
        type: 'épisode neigeux'
    },
    {
        numero: 3,
        type: 'douceur inhabituelle'
    },
    {
        numero: 4,
        type: 'épisode pluvieux'
    },
    {
        numero: 5,
        type: 'inondation'
    },
    {
        numero: 6,
        type: 'tempête/coup de vent'
    },
    {
        numero: 7,
        type: 'cyclone'
    },
    {
        numero: 8,
        type: 'chaleur / canicule'
    },
    {
        numero: 9,
        type: 'orages'
    },
    {
        numero: 10,
        type: 'sécheresse'
    },
    {
        numero: 11,
        type: 'pluies verglaçantes'
    },
    {
        numero: 12,
        type: 'gelées tardives'
    },
    {
        numero: 13,
        type: 'gelées précoces'
    },
    {
        numero: 14,
        type: 'froid'
    },
    {
        numero: 15,
        type: 'grêle'
    },
    {
        numero: 16,
        type: 'épisode neigeux tardif'
    },
    {
        numero: 17,
        type: 'épisode neigeux précoce'
    },
    {
        numero: 18,
        type: 'tornade'
    },
    {
        numero: 19,
        type: 'dépression extra-tropicale'
    },
    {
        numero: 20,
        type: 'tempête tropicale'
    },
    {
        numero: -1,
        type: 'autres'
    }
]


export const localisationData = [
    {
        numero: -1,
        region: 'autres'
    },
    {
        numero: 42,
        region: 'Alsace'
    },
    {
        numero: 72,
        region: 'Aquitaine'
    },
    {
        numero: 83,
        region: 'Auvergne'
    },
    {
        numero: 26,
        region: 'Bourgogne'
    },
    {
        numero: 53,
        region: 'Bretagne'
    },
    {
        numero: 21,
        region: 'Champagne-Ardenne'
    },
    {
        numero: 94,
        region: 'Corse'
    },
    {
        numero: 43,
        region: 'Franche-Comté'
    },
    {
        numero: 11,
        region: 'Île-de-France'
    },
    {
        numero: 91,
        region: 'Languedoc-Roussillon'
    },
    {
        numero: 74,
        region: 'Limousin'
    },
    {
        numero: 24,
        region: 'Centre'
    },
    {
        numero: 41,
        region: 'Lorraine'
    },
    {
        numero: 73,
        region: 'Midi-Pyrénées'
    },
    {
        numero: 31,
        region: 'Nord-Pas-de-Calais'
    },
    {
        numero: 25,
        region: 'Basse-Normandie'
    },
    {
        numero: 23,
        region: 'Haute-Normandie'
    },
    {
        numero: 52,
        region: 'Pays de la Loire'
    },
    {
        numero: 22,
        region: 'Picardie'
    },
    {
        numero: 54,
        region: 'Poitou-Charentes'
    },
    {
        numero: 93,
        region: 'PACA'
    },
    {
        numero: 82,
        region: 'Rhône-Alpes'
    }
]