import {
    meteoData,
    localisationData
} from "./dummy-data";

export const cleanData = (data) => {
    for (var i in data) {
        if (data[i].localisation !== "") {
            const idLocations = data[i].localisation.split(",")
            const regions = getRegions(idLocations)
            data[i].localisation = regions
        }

        if(data[i].type !== "") {
            const idTypes = data[i].type.split(",")
            const typeEvenements = getTypeEvenements(idTypes)
            data[i].type = typeEvenements
        }
    }
}

export const getRegions = (idLocations) => {
    const locations = getLocations(idLocations)
    const regions = checkIfExistReturnRegions(locations)
    return regions.join(",")
}

export const getLocations = (idLocations) => {
    const locations = []
    idLocations.forEach(id => {
        let region = localisationData.find(d => {
            return d.numero === parseInt(id)
        })
        locations.push(region)
    })

    return locations
}

export const checkIfExistReturnRegions = (data) => {
    return [...new Set(data.map(item => item.region))];
}

export const getTypeEvenements = (idTypes) => {
    const types = getTypes(idTypes)
    const typeEvenements = checkIfExistReturnTypes(types)

    return typeEvenements.join(",")
}

export const getTypes = (idTypes) => {
    const types = []
    idTypes.forEach(id => {
        let region = meteoData.find(d => {
            return d.numero === parseInt(id)
        })
        types.push(region)
    })
    return types
}

export const checkIfExistReturnTypes = (data) => {
    return [...new Set(data.map(item => item.type))];
}

export const filterDataByTypes = (data, type) => {
    // return data.filter(d => d.type.indexOf(type))
    console.log(type)
    console.log(data.filter(d => d.type.indexOf(type)))
    return data.filter(d => d.type.indexOf(type) === 0)
}