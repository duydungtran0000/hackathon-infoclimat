import React from "react";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import "./record-data.styles.js";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/styles";
import PropTypes from "prop-types";
import LineChart from "components/line-chart/line-chart.component";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { styles } from "./record-data.styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import axios from "axios";
import { uniqueCities, filterDataByCity } from "./record-data.utils";
import PrecipChart from "components/precip-chart/precip-chart.component.jsx";

class RecordDataPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      citySelected: "",
      data: null,
      isLoading: true,
      cities: null,
      resData: null,
      typeSelected: "temp",
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_URL}/api/HistoricNormales`)
      .then((response) => {
        console.log(response.data);
        if (response.status !== 200) {
          return;
        }
        const resData = response.data.datas;
        const cities = uniqueCities(resData);
        this.setState({
          isLoading: false,
          cities: cities,
          resData: resData,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  handleChange = (event) => {
    console.log(event.target);
    const { name, value } = event.target;
    this.setState(
      {
        ...this.state,
        [name]: value,
      },
      function () {
        if (this.state.citySelected !== "" && name === "citySelected") {
          const data = filterDataByCity(
            this.state.resData,
            this.state.citySelected
          );
          console.log("change");
          this.setState({
            ...this.state,
            data: data,
          });
        }
      }
    );
  };

  render() {
    const { classes } = this.props;
    console.log(this.state.data);
    return (
      <div className="record-data">
        <div>
          <Breadcrumbs aria-label="breadcrumb">
            <Link color="inherit" href="/">
              <Typography variant="h5">HistorIc</Typography>
            </Link>
            <Link color="inherit" href="/record-card">
              <Typography variant="h5">événements records</Typography>
            </Link>
          </Breadcrumbs>
        </div>
        <Paper className={classes.paper} elevation={3}>
          {this.state.isLoading ? (
            <CircularProgress />
          ) : (
            <div className={classes.wrapper}>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-city-native-simple">
                  Choisir la ville
                </InputLabel>
                <Select
                  native
                  value={this.state.citySelected}
                  onChange={(e) => this.handleChange(e)}
                  label="Ville"
                  inputProps={{
                    name: "citySelected",
                    id: "outlined-city-native-simple",
                  }}
                >
                  <option aria-label="None" value="" />
                  {this.state.cities.map((city, id) => (
                    <option key={id} value={city}>
                      {city}
                    </option>
                  ))}
                </Select>
              </FormControl>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-type-native-simple">
                  Type de données
                </InputLabel>
                <Select
                  native
                  value={this.state.typeSelected}
                  onChange={(e) => this.handleChange(e)}
                  label="Ville"
                  inputProps={{
                    name: "typeSelected",
                    id: "outlined-type-native-simple",
                  }}
                >
                  <option aria-label="None" value="temp">
                    Température
                  </option>
                  <option aria-label="None" value="precip">
                    Précipitation
                  </option>
                </Select>
              </FormControl>
              {this.state.data ? (
                <div className={classes.lineChart}>
                  {this.state.typeSelected === "temp" ? (
                    <LineChart data={this.state.data} />
                  ) : (
                    <PrecipChart data={this.state.data} />
                  )}
                </div>
              ) : (
                <h2>Veuillez sélectionner une ville et un type de données</h2>
              )}
            </div>
          )}
        </Paper>
      </div>
    );
  }
}

RecordDataPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecordDataPage);
