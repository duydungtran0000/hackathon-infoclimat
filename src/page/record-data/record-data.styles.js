export const styles = (theme) => ({
    root: {
        marginTop: "1.5rem"
    },
    paper: {
        minHeight: "60vh",
        marginTop: "5rem",
    },
    wrapper: {
        padding: "1.25rem",
    },
    lineChart: {
        marginTop: "1rem"
    },
    formControl: {
        minWidth: 300,
        marginRight: "1rem"
    },
});