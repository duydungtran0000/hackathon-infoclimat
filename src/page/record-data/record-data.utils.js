export const uniqueCities = (data) => {
    return [...new Set(data.map(item => item.nom_ref))];
} 

export const filterDataByCity = (data, city) => {
    return data.filter(d => d.nom_ref === city)
}